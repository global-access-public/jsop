
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module Data.JSOP.Example where

import Data.Aeson
import Data.Aeson.Lens
import Data.String.Interpolate
import Generics.SOP
import Generics.SOP.TH
import Data.JSOP
import Protolude hiding (All, optional, (:*:))
import Data.Maybe (fromJust)

data ABC = ABC Text Int Int deriving (Show, Eq)

deriveGeneric ''ABC

cherryPickABC :: NP (JSOP Key) '[Text, Int, Int]
cherryPickABC =
  required "object 1 / a string" _String
    :* required "object 2 / a number" _Integral
    :* optional "object 4 / a number" 42 _Integral
    :* Nil

jsonWithABC :: Value
jsonWithABC = fromJust . decode $ [i| 
  {
    "object 1": 
      { "a string": "ciao"
      , "ignore me" : 34
      }
  , "object 2": 
      { "a number": 2
      , "object 3": {}
      }
  , "object 4": {
      "a plumber" :43
      } 
  }
  |]

abc :: ABC
abc = fromRight (panic "jread failed") $ jread (ksplit (" / " :: Key)) cherryPickABC jsonWithABC

jsonABC :: Value 
jsonABC = jwrite (ksplit " / ") cherryPickABC (Just jsonWithABC) (ABC "mamma" 44 103) 
